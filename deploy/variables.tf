variable "prefix" {
  default = "raad"
}
# ... existing file ...

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "contact@email.com"
}

# ... existing code ...

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
# ... existing code ...


variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion" # ... This name should macth the key name created on AWS console "EC2/Key Pairs"
}

# ... existing code ...

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "415519600396.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "415519600396.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
