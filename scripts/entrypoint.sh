#!/bin/sh

set -e  # Exit if any errors occurs
python manage.py collectstatic --noinput 
                                        #  In Django applications, its best practice to serve static file through proxy
                                        # uWSGI is good for serving .py files
                                        # collectstatic command collects all the static files and store in single directory */
python manage.py wait_for_db
python manage.py migrate  # Runs database migration

# Run uWSGI service , 4 workers in container , runs as master service, 
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
